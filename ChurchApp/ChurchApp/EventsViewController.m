//
//  EventsViewController.m
//  ChurchApp
//
//  Created by Seattle App Lab Pvt Ltd on 30/11/13.
//  Copyright (c) 2013 Seattle App Lab Pvt Ltd. All rights reserved.
//

#import "EventsViewController.h"
#import "EventDetailsViewController.h"

@interface EventsViewController ()

@end

@implementation EventsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Events";
}

-(IBAction)push_EventDetailsScreen
{
    EventDetailsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailsScreen"];
    controller.PageTitle = @"All Events Details";
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
