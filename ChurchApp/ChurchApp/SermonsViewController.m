//
//  SermonsViewController.m
//  ChurchApp
//
//  Created by Seattle App Lab Pvt Ltd on 30/11/13.
//  Copyright (c) 2013 Seattle App Lab Pvt Ltd. All rights reserved.
//

#import "SermonsViewController.h"
#import "SermonsListViewController.h"

@interface SermonsViewController ()

@end

@implementation SermonsViewController
@synthesize TblSermons;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Sermons";
    
    ArrSermons = [[NSMutableArray alloc] init];
    [ArrSermons addObject:@"Seismic"];
    [ArrSermons addObject:@"Presence"];
    [ArrSermons addObject:@"Welcome Home"];
    [ArrSermons addObject:@"Faith Home"];
    [ArrSermons addObject:@"Ideal Family"];

    TblSermons.delegate = self;
    TblSermons.dataSource = self;
    [TblSermons reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    TblSermons.contentOffset = CGPointMake(0, 0);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        return 120;
    }
    else
    {
        return 50;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ArrSermons count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        if(indexPath.row==0)
        {
            
            UIView *ViewContent = [[UIView alloc] init];
            ViewContent.backgroundColor = [UIColor whiteColor];
            ViewContent.layer.cornerRadius = 5.0;
            ViewContent.layer.borderWidth = 3.0;
            ViewContent.layer.borderColor = [UIColor darkGrayColor].CGColor;
            
            UIImageView *ImgContent = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 100)];
            ImgContent.image = [UIImage imageNamed:@"sermons-icon.png"];
            ImgContent.backgroundColor = [UIColor whiteColor];
            [ViewContent addSubview:ImgContent];
            
            ViewContent.frame = CGRectMake(10, 10, 300, 100);
            [cell.contentView addSubview:ViewContent];
            
            ViewContent = nil;
            
            cell.backgroundColor = [UIColor blackColor];
        }
        else
        {
            cell.imageView.image = [UIImage imageNamed:@"sermons-icon.png"];
            
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.backgroundColor = [UIColor blackColor];
            cell.textLabel.text = [ArrSermons objectAtIndex:indexPath.row-1];
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    
    if(indexPath.row==0)
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SermonsListViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SermonsListScreen"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
