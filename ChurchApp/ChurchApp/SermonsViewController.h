//
//  SermonsViewController.h
//  ChurchApp
//
//  Created by Seattle App Lab Pvt Ltd on 30/11/13.
//  Copyright (c) 2013 Seattle App Lab Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SermonsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *ArrSermons;
}
@property(nonatomic,strong) IBOutlet UITableView *TblSermons;

@end
