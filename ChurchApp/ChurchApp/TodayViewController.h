//
//  TodayViewController.h
//  ChurchApp
//
//  Created by Seattle App Lab Pvt Ltd on 30/11/13.
//  Copyright (c) 2013 Seattle App Lab Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TodayViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    
}
@property(nonatomic,strong) IBOutlet UITableView *TblInfo;

@end
