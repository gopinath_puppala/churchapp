//
//  TodayViewController.m
//  ChurchApp
//
//  Created by Seattle App Lab Pvt Ltd on 30/11/13.
//  Copyright (c) 2013 Seattle App Lab Pvt Ltd. All rights reserved.
//

#import "TodayViewController.h"

@interface TodayViewController ()

@end

@implementation TodayViewController
@synthesize TblInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Today";
    
    TblInfo.delegate = self;
    TblInfo.dataSource = self;
    [TblInfo reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    TblInfo.contentOffset = CGPointMake(0, 0);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        NSString *strText = @"Let no man say when he is tempted, I am templted of God; for God cannot be tempted with evil, and he himself tempteth no man: but each man is tempted, when he is drawn away by this own just, and enticed.";
        CGSize maxTextSize = [strText sizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0] constrainedToSize:CGSizeMake(280, 10000) lineBreakMode:NSLineBreakByWordWrapping];
        
        return maxTextSize.height+30;
    }
    else{
        return 40;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 11;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
     
        if(indexPath.row==0)
        {
            
            UIView *ViewContent = [[UIView alloc] init];
            ViewContent.backgroundColor = [UIColor whiteColor];
            ViewContent.layer.cornerRadius = 5.0;
            ViewContent.layer.borderWidth = 3.0;
            ViewContent.layer.borderColor = [UIColor darkGrayColor].CGColor;
            
            NSString *strText = @"Let no man say when he is tempted, I am templted of God; for God cannot be tempted with evil, and he himself tempteth no man: but each man is tempted, when he is drawn away by this own just, and enticed.";
            CGSize maxTextSize = [strText sizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0] constrainedToSize:CGSizeMake(280, 10000) lineBreakMode:NSLineBreakByWordWrapping];
            
            
            UILabel *LblText = [[UILabel alloc] init];
            LblText.text = strText;
            LblText.frame = CGRectMake(10, 10, 280, maxTextSize.height);
            LblText.font =[UIFont fontWithName:@"Helvetica Nue-Bold" size:18.0];
            LblText.backgroundColor = [UIColor clearColor];
            LblText.textColor = [UIColor blackColor];
            LblText.numberOfLines = 100;
            [ViewContent addSubview:LblText];
            LblText = nil;
            
            ViewContent.frame = CGRectMake(10, 10, 300, maxTextSize.height+10);
            [cell.contentView addSubview:ViewContent];
            
            ViewContent = nil;
            
            cell.backgroundColor = [UIColor blackColor];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        else if(indexPath.row==1)
        {
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.backgroundColor = [UIColor blackColor];
            cell.textLabel.text = @"Event1";
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
            cell.detailTextLabel.text = @"6:00AM";

            cell.accessoryType = UITableViewCellAccessoryDetailButton;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        else if(indexPath.row==2)
        {
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.backgroundColor = [UIColor blackColor];
            cell.textLabel.text = @"Event2";
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
            cell.detailTextLabel.text = @"6:30AM";
            
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        else if(indexPath.row==3)
        {
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.backgroundColor = [UIColor blackColor];
            cell.textLabel.text = @"Event3";
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
            cell.detailTextLabel.text = @"7:00AM";
            
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        else if(indexPath.row==4)
        {
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.backgroundColor = [UIColor blackColor];
            cell.textLabel.text = @"Event4";
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
            cell.detailTextLabel.text = @"7:30AM";
            
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        else if(indexPath.row==5)
        {
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.backgroundColor = [UIColor blackColor];
            cell.textLabel.text = @"Event5";
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
            cell.detailTextLabel.text = @"8:00AM";
            
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        else if(indexPath.row==6)
        {
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.backgroundColor = [UIColor blackColor];
            cell.textLabel.text = @"Event6";
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
            cell.detailTextLabel.text = @"8:30AM";
            
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        else if(indexPath.row==7)
        {
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.backgroundColor = [UIColor blackColor];
            cell.textLabel.text = @"Event7";
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
            cell.detailTextLabel.text = @"9:00AM";
            
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        else if(indexPath.row==8)
        {
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.backgroundColor = [UIColor blackColor];
            cell.textLabel.text = @"Event8";
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
            cell.detailTextLabel.text = @"9:30AM";
            
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        else if(indexPath.row==9)
        {
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.backgroundColor = [UIColor blackColor];
            cell.textLabel.text = @"Event9";
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
            cell.detailTextLabel.text = @"10:00AM";
            
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }

        else{
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.backgroundColor = [UIColor blackColor];
            cell.textLabel.text = @"Event10";
            cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
            cell.detailTextLabel.text = @"10:30AM";
            
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    
    
    return cell;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
