//
//  SermonsListViewController.m
//  ChurchApp
//
//  Created by Seattle App Lab Pvt Ltd on 30/11/13.
//  Copyright (c) 2013 Seattle App Lab Pvt Ltd. All rights reserved.
//

#import "SermonsListViewController.h"
#import "SermonDetailViewController.h"

@interface SermonsListViewController ()

@end

@implementation SermonsListViewController
@synthesize TblSermons;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Sermons List";
    
    ArrSermons = [[NSMutableArray alloc] init];
    [ArrSermons addObject:@"Paul"];
    [ArrSermons addObject:@"Esther"];
    [ArrSermons addObject:@"Daniel 1:1"];
    [ArrSermons addObject:@"Isaiah"];
    [ArrSermons addObject:@"Elijah"];
    [ArrSermons addObject:@"Huldah Buntain"];
    [ArrSermons addObject:@"Dave Donaldson of Convov of Hope"];
    [ArrSermons addObject:@"Seismic Pt. 11"];
    [ArrSermons addObject:@"Seismic Pt. 1"];

    TblSermons.delegate = self;
    TblSermons.dataSource = self;
    [TblSermons reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    TblSermons.contentOffset = CGPointMake(0, 0);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        return 100;
    }
    else
    {
        return 50;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ArrSermons count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(indexPath.row==0)
    {
        UIImageView *ImgContent = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
        ImgContent.image = [UIImage imageNamed:@"sermons-icon.png"];
        ImgContent.backgroundColor = [UIColor whiteColor];
        [cell.contentView addSubview:ImgContent];
    
        cell.backgroundColor = [UIColor blackColor];
    }
    else
    {
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.backgroundColor = [UIColor blackColor];
        cell.textLabel.text = [ArrSermons objectAtIndex:indexPath.row-1];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Seismic Impact of the %@",[ArrSermons objectAtIndex:indexPath.row-1]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    if(indexPath.row==0)
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    return cell;
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SermonDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SermonDetailsScreen"];
    [self.navigationController pushViewController:controller animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
