//
//  SermonDetailViewController.m
//  ChurchApp
//
//  Created by Seattle App Lab Pvt Ltd on 30/11/13.
//  Copyright (c) 2013 Seattle App Lab Pvt Ltd. All rights reserved.
//

#import "SermonDetailViewController.h"

@interface SermonDetailViewController ()

@end

@implementation SermonDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Sermon Info";
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
